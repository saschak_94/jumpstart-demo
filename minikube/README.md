# Deploy Jumpstart Demo app on Kubernetes (Minikube)


[Minikube](https://github.com/kubernetes/minikube) runs a single-node Kubernetes cluster inside a VM on your laptop. It is designed to easily test applications locally.
Be aware that everything what we do in the next steps is done on **one** node (VM) but it can be used on a multi-node real world
Kubernetes cluster as well. When working with Kubernetes, you actually don't really care about the number of nodes it has, 
therefore minikube is perfect to try out Kubernetes and develop with it day-to-day.  

## Download and start Minikube

Before we can start working with Minikube we have to download the binary, which is a CLI tool that is used to create/configure a Minkube "cluster".

The next steps assumes we are working on Windows.

1. Download Minkube: https://storage.googleapis.com/minikube/releases/latest/minikube-windows-amd64.exe
2. rename the downloaded file to `minikube.exe`
3. add this file to your path: In a shell (e.g. Git bash) execute: 
    
    ```shell
    export PATH=$PATH:/path/to/minikube.exe
    ```
4.  verify that minkube has been successfully installed: In a shell execute
    
    ```shell
    minikube version
    ```
    
    where you should see an output similar to
    
    ```shell
    minikube version: v0.25.0
    ```
5. Now we can start minikube, which means we create a local 1-node Kubernetes cluster. Execute
  ```shell
  minikube start --vm-driver virtualbox
  ```
  This should give an output similar to
  ```
  Starting local Kubernetes v1.9.0 cluster...
  Starting VM...
  Getting VM IP address...
  Moving files into cluster...
  Setting up certs...
  Connecting to cluster...
  Setting up kubeconfig...
  Starting cluster components...
  Kubectl is now configured to use the cluster.
  Loading cached images from config file.
  ```

### Possible Problems and solutions

It can happen that you get some errors when starting minikube on Windows. Make sure the following is correctly configured:

* Hyper-V is disabled: [HowTo](http://www.poweronplatforms.com/enable-disable-hyper-v-windows-10-8/)
* enable VT-x: [HowTo on HP](https://h30434.www3.hp.com/t5/Desktop-Hardware-and-Upgrade-Questions/How-to-Enable-Intel-Virtualization-Technology-vt-x-on-HP/td-p/3198063)

## Download and install kubectl

The [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/) is the Kubernetes command line tool to interact with any Kubernetes cluster. 
It provides an API to talk to both Minikube as well as to Kubernetes cluster of any size.

1. [Download kubectl for Windows](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
2. add kubectl to your path in order to be able to execute it anywhere ony your computer (see [this link](http://www.itprotoday.com/management-mobility/how-can-i-add-new-folder-my-system-path) how to do it on Windows)
3. Verify the installation with 
    ```
    kubectl version
    ```

    which should print something like 

    ```
    Client Version: version.Info{Major:"1", Minor:"9", GitVersion:"v1.9.3", GitCommit:"d2835416544f298c919e2ead3be3d0864b52323b", GitTreeState:"clean", BuildDate:"2018-02-09T21:51:54Z", GoVersion:"go1.9.4", Compiler:"gc", Platform:"darwin/amd64"}
    Server Version: version.Info{Major:"", Minor:"", GitVersion:"v1.9.0", GitCommit:"925c127ec6b946659ad0fd596fa959be43f0cc05", GitTreeState:"clean", BuildDate:"2018-01-26T19:04:38Z", GoVersion:"go1.9.1", Compiler:"gc", Platform:"linux/amd64"}
    ```
Now we can start using `kubectl` to work with minikube.

## Create namespace

We don't want to deploy our application to the `default` namespace. We create a new one where we deploy everything:

```
kubectl create namespace jumpstart
```

Alternatively you can also use the short version `kubectl create ns jumpstart`.

## Create Deployment

Create a simple file called `jumpstart-demo.yaml` with the content

```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  labels:
    app: hello-react
  name: jumpstart-demo
  namespace: jumpstart
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hello-react
  template:
    metadata:
      labels:
        app: hello-react
    spec:
      containers:
      - image: registry.gitlab.com/saschak_94/jumpstart-demo
        name: jumpstart-demo
```

Let's have a look into this file:

* It is a Kubernetes deployment, indicated by the `Kind` field.
* A deployment named `jumpstart-demo` will be created, indicated by the `metadata:name` field.
* The deployment and the corresponding Pods will be deployed in the namespace `jumpstart`.
* The Deployment creates one Pod, indicated by the `replicas` field.
* The selector field defines how the Deployment finds which Pods to manage. In this case, we simply select on one label defined in the Pod template (app: hello-react). However, more sophisticated selection rules are possible, as long as the Pod template itself satisfies the rule.
* The Pod template’s specification, or `template:spec` field, indicates that the Pod run one container which runs the latest version of the Docker image `saschak_094/jumpstart-demo` from the Gitlab Registry (**your image name may vary here!**).

Now let's create the deployment with

```
kubectl apply -f jumpstart-demo.yaml
```

What happens? Let's check whether the app has been successfully deployed.

First, we check the Kubernetes deployment in the namespace `jumpstart`:

```shell
$ kubectl get deployment -n jumpstart
NAME             DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
jumpstart-demo   1         1         1            0           14s
```

We can see, the `Desired` state is `1`, which means we want to have 1 Pod running. This is correct, as we have indicated in thee deployment manifest `replicas: 1`. `Current` means, that in fact, there is 1 Pod running (we will also see this in a minute with another command). `UP-TO-DATE` indicates that the Pods definition is up to date with that defined in the deployment manifest. 

OH! `Available:0` indicates the Pod is not available. Something seems to go wrong, so let's check the Pod:

```
kubectl get pod -n jumpstart
NAME                              READY     STATUS         RESTARTS   AGE
jumpstart-demo-55fc8c9b84-9b4cc   0/1       ErrImagePull   0          6m
```

Indeed, we can see that no Pod is ready and the Status is `ErrImagePull`, so something went wrong when trying to pull the image. 

We can debug this problem with the `kubectl describe` method. This method can be used for almost every Kubernetes object. So, let's check that pod

```shell
$ kubectl describe pod jumpstart-demo-55fc8c9b84-9b4cc -n jumpstart
Name:           jumpstart-demo-55fc8c9b84-9b4cc
Namespace:      jumpstart
Node:           minikube/192.168.99.100
Start Time:     Mon, 19 Feb 2018 09:20:23 +0100
Labels:         app=hello-react
                pod-template-hash=1197475640
Annotations:    <none>
Status:         Pending
IP:             172.17.0.5
Controlled By:  ReplicaSet/jumpstart-demo-55fc8c9b84
Containers:
  jumpstart-demo:
    Container ID:
    Image:          registry.gitlab.com/saschak_94/jumpstart-demo
    Image ID:
    Port:           <none>
    State:          Waiting
      Reason:       ImagePullBackOff
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-z5mmp (ro)
Conditions:
  Type           Status
  Initialized    True
  Ready          False
  PodScheduled   True
Volumes:
  default-token-z5mmp:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-z5mmp
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     <none>
Events:
  Type     Reason                 Age               From               Message
  ----     ------                 ----              ----               -------
  Normal   Scheduled              9m                default-scheduler  Successfully assigned jumpstart-demo-55fc8c9b84-9b4cc to minikube
  Normal   SuccessfulMountVolume  9m                kubelet, minikube  MountVolume.SetUp succeeded for volume "default-token-z5mmp"
  Normal   Pulling                7m (x4 over 8m)   kubelet, minikube  pulling image "registry.gitlab.com/saschak_94/jumpstart-demo"
  Warning  Failed                 7m (x4 over 8m)   kubelet, minikube  Failed to pull image "registry.gitlab.com/saschak_94/jumpstart-demo": rpc error: code = Unknown desc = Error response from daemon: Get https://registry.gitlab.com/v2/saschak_94/jumpstart-demo/manifests/latest: denied: access forbidden
  Warning  Failed                 7m (x4 over 8m)   kubelet, minikube  Error: ErrImagePull
  Normal   BackOff                6m (x6 over 8m)   kubelet, minikube  Back-off pulling image "registry.gitlab.com/saschak_94/jumpstart-demo"
  Warning  Failed                 3m (x17 over 8m)  kubelet, minikube  Error: ImagePullBackOff
```

We can see the error message:

```
Failed to pull image "registry.gitlab.com/saschak_94/jumpstart-demo": rpc error: code = Unknown desc = Error response from daemon: Get https://registry.gitlab.com/v2/saschak_94/jumpstart-demo/manifests/latest: denied: access forbidden
```

There is an authentication problem when trying to pull the Docker image `saschak_94/jumpstart-demo` from the Gitlab registry. 


If you see this error, you have two options to solve it:

1. make your Gitlab Project [publicly available](https://docs.gitlab.com/ee/public_access/public_access.html)
2. you need to pass authentication credentials to [pull an image from a private registry](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)



## Access App in your Browser

There are two way to do that:

1. In a real world scenario we would need to create a Kubernetes `service` which will be used to expose the pod to the public internet. 
  
    For this we would need a Public IP/Public DNS assigned to that service. This can be done easily with a public cloud provider.
    
    As we are working with minikube which is running on our local notebook, we can't do that.
    
    But we can port-forward the deployment. What does this mean? The dockerized application exposes the port 3000, which means the application runs on port 3000. `kubectl` has a method to forward a custom local port (a port that is free to use on your local notebook) to the container port, i.e. the port where the application is running on. This makes it possible to access the app in the container via `http://localhost:<localPort>`.
    
    Let's do this:

    ```shell
    kubectl port-forward <pod-name> 3000:3000
    ```

    This command forwards the local port 3000, indicated by the first 3000, to the container (target) port 3000, indicated by the second 3000. 

    Now open a Browser on the URL `http://localhost:3000`. Tataaaaaa, our beautiful app! :)

2. Create a [Kubernetes Service](https://kubernetes.io/docs/concepts/services-networking/service/) by exposing the deployment
  
    ```shell
    kubectl expose deployment hello-react --port=3000 --type=NodePort
    ```
    Check whether your service has been created:
    ```shell
    $ kubectl get svc
    NAME             TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
    jumpstart-demo   NodePort    10.109.63.172   <none>        3000:30439/TCP   5s
    ```
    where we can see that the service has been successfully created and the container port 3000 has been mapped to the node port 30439 (This may vary in your case). 

    Now we can access this service over the Minikube's IP address, which we can find with
    ```shell
    $ minikube status
    minikube: Running
    cluster: Running
    kubectl: Correctly Configured: pointing to minikube-vm at 192.168.99.100
    ``` 
    So browsing in Chrome to http://192.168.99.100:30439/ will show us our awesome application running on minikube.
